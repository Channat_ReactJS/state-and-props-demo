import React from 'react'
import {Card} from 'react-materialize'

function MyCard(props) {

    return (
        <div>
            <Card title={props.cardTitle} >
                {props.children}
            </Card>
        </div>
    )
}

export default MyCard
import React, {Component} from 'react'
import {Button} from 'react-materialize'


class Welcome extends Component {

    state = {
        name: 'Fong',
        age: 30,
        gender: 'M',
        description: 'old description'
    }

    updateAge() {
        // Synchronus
       // this.setState({
       //     age: this.state.age + 1
       // })

        // Asyn
        this.setState((preState, props) => {
            console.log(props.description)
            return {
                age: preState.age + 1,
                description: props.description
            }
        })
    }


    render() {
        const {name, age} = this.state
        console.log(this.props)
        return (
            <div>
                <h3>Welcome {name}</h3>
                <h3>Age: {age}</h3>
                <h5>{this.props.description}</h5>
                <p>{this.props.anotherDesc}</p>
                <Button onClick={() => this.updateAge()} waves='light'>Update Age</Button>
            </div>
        )

    }



}
Welcome.defaultProps = {
    description: "Default Descrition",
    anotherDesc: "Default another descrition"
}
export default Welcome


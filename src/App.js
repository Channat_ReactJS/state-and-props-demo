import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Welcome from "./components/Welcome";
import MyCard from "./components/MyCard";

class App extends Component {

    state = {
        title: 'CS6'
    }

    render() {
        return (
            <div className="App">

                <MyCard cardTitle={this.state.title}/>

                <MyCard cardTitle='Research a lot...'>
                    <Welcome />
                </MyCard>

            </div>
        );
    }
}

export default App;
